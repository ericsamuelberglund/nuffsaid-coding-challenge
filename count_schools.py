import csv

LCITY05 = 4 # City Column Index
LSTATE05 = 5 # State Column Index
MLOCALE = 8  # Metro-centric Locale Column Index

def print_counts():
    entries = get_data()

    # Get the total number of schools / entries
    total_schools = len(entries)
    print(f"Total Schools: {total_schools}")

    # Loop through entries to generate school count by location data
    schools_per_state = {}
    schools_per_locale = {}
    schools_per_city = {}
    for entry in entries:
        state, locale, city = entry[LSTATE05], entry[MLOCALE], entry[LCITY05]
        if state not in schools_per_state:
            schools_per_state[state] = 1
        else:
            schools_per_state[state] +=1
        if locale not in schools_per_locale:
            schools_per_locale[locale] = 1
        else:
            schools_per_locale[locale] += 1
        if city not in schools_per_city:
            schools_per_city[city] = 1
        else:
            schools_per_city[city] += 1

    print("Schools by State:")
    for state, num in sorted(schools_per_state.items()):
        print(f"{state}: {num}")

    print("Schools by Metro-centric Locale:")
    for locale, num in sorted(schools_per_locale.items()):
        print(f"{locale}: {num}")

    # Get the maximum number of schools in a city, then find the city (or cities) with that number
    max_schools_per_city = max(schools_per_city.values())
    max_cities = [city for city, num in schools_per_city.items() if num == max_schools_per_city]
    print(f"City(s) with most schools: { max_cities }, {max_schools_per_city} schools")

    # Find the total number of unique cities with a school
    unique_cities = len(schools_per_city)
    print(f"Unique cities with at least one school: {unique_cities}")

def get_data():
    # Read in the CSV file and store each entry in a list
    with open("school_data.csv", encoding="ISO-8859-1") as school_data:
        next(school_data)  # Skip CSV Header Line
        reader = csv.reader(school_data)
        return [entry for entry in reader]

if __name__ == '__main__':
    print_counts()
