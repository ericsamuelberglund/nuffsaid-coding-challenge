import csv
import time

SCHOOL_NAME = 0
CITY = 1
STATE = 2

def get_data():
    # Read in the CSV file and store each entry in a list
    with open("school_data.csv", encoding="ISO-8859-1") as school_data:
        next(school_data)  # Skip CSV Header Line
        reader = csv.reader(school_data)
        return [entry[3:6] for entry in reader]

def search_schools(query):
    entries = get_data()
    keywords = sanitize_query(query)

    # Generate matching entries, wrap to determine query time
    start_time = time.time()
    matching_entries = generate_matches(entries, keywords)
    end_time = time.time()

    # Display query results and timings
    total_time = round(end_time - start_time, 3)
    total_time_ms = total_time * 1000
    print(f'\nResults for "{query}" (search took {total_time}s, {total_time_ms}ms)')
    count = 0
    for entry in matching_entries:
        if count > 2:
            break
        print(f'{count + 1}. {matching_entries[count][0][SCHOOL_NAME]}')
        print(f'{matching_entries[count][0][CITY]}, {matching_entries[count][0][STATE]}')
        count += 1

def sanitize_query(query):
    query = query.upper()
    # Strip "SCHOOL" because it generates tons of false matches
    query.replace("SCHOOL", "")
    return query.split()

def generate_matches(entries, keywords):
    matching_entries = []
    for entry in entries:
        matches = 0
        rank = 0
        name_match_count = 0

        # Loop through keywords, keeping track of the number of keywords that match
        for keyword in keywords:
            match = False
            # Strip "SCHOOL" because it generates tons of false matches
            if keyword in entry[SCHOOL_NAME].replace("SCHOOL", ""):
                match = True
                name_match_count += 1
            if keyword in entry[STATE]:
                match = True
            if keyword in entry[CITY]:
                match = True
            if match:
                rank += 1

        # Matching school names weighted more heavily than other matches
        rank += name_match_count

        # Append the match and the rank
        if rank > 0:
            matching_entries.append([entry, rank])

    # Sort matching entries by highest rank
    return sorted(matching_entries, key=lambda x: x[1], reverse=True)

if __name__ == '__main__':
    search_schools("elementary school highland park")
    search_schools("jefferson belleville")
    search_schools("riverside school 44")
    search_schools("granada charter school")
    search_schools("foley high alabama")
    search_schools("KUSKOKWIM")
